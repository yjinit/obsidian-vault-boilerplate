
## 今天

### 今天待办

```dataview
TASK
WHERE !fullyCompleted AND due = date(today)
GROUP BY section
SORT due DESC
```

### 过期待办

```dataview
TASK 
WHERE !fullyCompleted AND due < date(today)
GROUP BY section
```

## 近期


### 7天内待办

```dataview
TASK 
WHERE !fullyCompleted AND due >= date(today) AND due <= date(today)+dur(6 d)
GROUP BY due
SORT due ASC
```

## 以后


### 全年待办

```dataview
TASK 
WHERE !fullyCompleted AND due >= date(soy) AND due <= date(eoy)
GROUP BY section
```
