
## obsidian-vault-boilerplate

obsidian-vault-boilerplate 是一个打包的 obsidian  文件夹，提供了一些初始的obsidian配置及插件，方便快速的新建一个obsidian vault。

- 当前版本为0.2，发布日期：[[2022-03-12]] 。
- 详细使用文档见 [[obsidian-vault-boilerplate-doc]] 。


## 关于作者 vitamind3

- Blog：[https://vitamind3.gitee.io/](https://vitamind3.gitee.io/)
- gitee首页：[https://gitee.com/vitamind3](https://gitee.com/vitamind3)
- 知乎主页：[https://www.zhihu.com/people/vitamind3](https://www.zhihu.com/people/vitamind3)
