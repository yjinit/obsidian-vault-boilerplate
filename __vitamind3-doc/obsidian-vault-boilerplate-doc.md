---
number headings: auto, first-level 2, max 6, _.1.1
---



# boilerplate vault模板说明

obsidian-vault-boilerplate 是一个打包的 obsidian  文件夹，提供了一些初始的obsidian配置及插件，方便快速的新建一个obsidian vault。

当前版本发布日期：[[2022-03-12]] 。

## 1 vault使用说明

### 1.1 默认配置

对 obsidian 默认配置的设置如下：

- 新建文件保存在 `__inbox` 文件夹下。
- 启用 wikilinks ，且路径为 shortest path 。
- 删除的文件在 .trash 文件夹下。

### 1.2 图片相关

- 拷贝粘贴图片：图片会保存在当前文件的同名assets文件夹下，例如当前文件 obsidian-vault-boilerplate-doc 的附件均在 obsidian-vault-boilerplate-doc-assets 下。
- 查看图片：把鼠标放图片上点击即可打开图片浏览窗口，可放大、缩小。
	- obsidian本身不带此功能，通过 Image Toolkit 插件实现。

### 1.3 表格

- 使用 Advanced Table toolbar 编辑表格。

### 1.4 调整文件结构

- 使用 Number Headings 系列命令为标题加数字编号。
- 使用 Note Refactor 拆分文档。
	- 注意：如果在文档中的代码段即` ``` `中包含有标题例如 `#` ，则拆分会出错。应该是插件BUG。

### 1.5 发布单个文件为html

obsidian 本身自带了 publish pdf 的功能，但如果想发布为 html，则推荐的方法如下：

1. 如果有图片，则使用Link Converter插件把链接转为md格式，插件选项中 Converted Link Path 设置 relative path 或 absolute path 。
2. 使用 Edge 浏览器（安装 Markdown Viewer插件）打开md文件，即可正常看到。

发布为html的好处是，可以方便的转PDF，粘贴拷贝进WORD等。

### 1.6 任务管理

添加任务的方法：

- 直接在任何一个笔记中添加任务。
- 通过 Perioic Note 插件在 `__schedules` 目录下创建年、月、季笔记，在其中添加任务。

查看当前任务列表：

- 待办事项 [[todo-list]]
- 完成的事项 [[done-list]]

## 2 文件夹

obsidian-vault-boilerplate 自带的文件夹均以下划线开头。

- `__dashboard`: 统计页面。
- `__inbox` : 新建的笔记默认存放在这里。
- `__journals`: 日志
- `__schedules`: 年计划、月计划、周计划。
- `__templates`: 模板文件。
- `__vitamind3-doc`: 当前项目的文档。


## 3 插件

obsidian-vault-boilerplate 当前版本包含的插件如下：

- Advanced Slides 1.14.3
- Advanced Tables 0.18.1
- Calendar 2.0.0 beta
- Custom Attachment Location 0.0.9
- Dataview 0.5.53
- Excalidraw 1.8.9
- File Info Panel 1.2.5
- Folder Note 0.7.3
- Full Calendar 0.9.5
- Hover Editor 0.11.8
- Image Toolkit 1.3.1
- Kanban 1.5.1
- Mermaid Tools 0.0.4
- Note Refactor 1.7.1
- Number Headings 1.13.0
- Obsidian Link Converter 0.1.4
- Periodic Notes 0.0.17
- Recent Files 1.3.1
- Style Settings 0.4.12
- Templater 1.16.0
- Workbench 1.6.1


## 4 主题

obsidian-vault-boilerplate 的主题包含：

- Default , obsidian 自带主题。
- Blue Topaz , 本模板库默认使用此主题。
	- 进入 Style Setting 插件，可选择多种不同的配色风格。
- Red Graphite , 可以的。
- Reverie , 可以的。
- Shimmering Focus , 我觉得挺好看，但是小白不要选这个主题。会隐藏掉左侧工具栏，这一点对小白非常不友好。


## 5 截屏

- 待办事项 ![[Pasted image 20220211222602.png]]
- 已经完成事项 ![[Pasted image 20220211222640.png]]

## 6 升级说明

### 6.1 0.3版

- 发布日期：2023-01-10
- 插件变更：
  - 添加：
    - obsidian-full-calendar 0.9.5 ，记事件还是有点用的。
    - obsidian-mermaid  , 用 mermaid 作画时更简单一点。
  - 更新：
    - dataview 0.5.53
    - Custom Attachment Location 0.0.9
    - number-headings-obsidian 1.13.0
    - advanced-tables-obsidian 0.18.1
    - obsidian-hover-editor 0.11.8
    - obsidian-image-toolkit-1.3.1
    - obsidian-kanban 1.5.1
    - obsidian-periodic-notes 1.0.0-beta.3
    - obsidian-style-settings 0.4.12
    - Templater1.16.0
  - 删除：
    - metaedit , 硬核玩家自己装，一般用户用不上。
    - obsidian-advanced-uri, 同上。
    - obsidian-emoji-toolbar , 用 `win + .` 输入。
    - obsidian-enhancing-mindmap , 直接画了脑图截图
    - obsidian-excalidraw-plugin, 直接画好截图。
    - obsidian-pandoc , 一般还真不怎么用。直接存pdf 。
    - obsidian-text-expand , 暂时用不上。 
- 样式更新：
    - 更新 blue topaz 


### 6.2 0.2版

- 发布日期： 2022-03-12
- 主要变更：
	- 删除 `__attachments` 文件夹。
	- 删除插件： Wikilinks to MDLinks 0.0.12 ，改为使用 Obsidian Link Converter
	- 更新、添加插件。

### 6.3 0.1版

- 发布日期： 2022-02-11


## 7 关于作者 vitamind3

- Blog：[https://vitamind3.gitee.io/](https://vitamind3.gitee.io/)
- gitee首页：[https://gitee.com/vitamind3](https://gitee.com/vitamind3)
- 知乎主页：[https://www.zhihu.com/people/vitamind3](https://www.zhihu.com/people/vitamind3)

